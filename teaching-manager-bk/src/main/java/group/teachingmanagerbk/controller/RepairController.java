package group.teachingmanagerbk.controller;

import group.teachingmanagerbk.service.RepairService;
import group.teachingmanagerbk.utils.ReturnResult.Result;
import group.teachingmanagerbk.vo.repair.LabRepair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class RepairController {

    @Autowired
    RepairService repairService;

    @GetMapping("/repair")
    public Result getLabRepair(String currentTerm, String curId) {
        ArrayList<LabRepair> data = repairService.getLabRepair(currentTerm, curId);
        return new Result().success(data);
    }

    @PutMapping("/repair/start")
    public Result startLabRepair(int equipmentRepairId) {
        try {
            repairService.startRepair(equipmentRepairId);
            return new Result().success();
        } catch (Exception e) {
            return new Result().error(e.getMessage());
        }
    }

    @PutMapping("/repair/finish")
    public Result finishLabRepair(int repairStateId, String descriptionOfRepair, int equipmentRepairId) {
        try {
            repairService.finishRepair(repairStateId, descriptionOfRepair, equipmentRepairId);
            return new Result().success();
        } catch (Exception e) {
            return new Result().error(e.getMessage());
        }
    }

    @GetMapping("/get/repair")
    public Result getRepairById(Integer repairId){
        LabRepair labRepair  = repairService.getRepairById(repairId);
        return new Result().success(labRepair);
    }
}
