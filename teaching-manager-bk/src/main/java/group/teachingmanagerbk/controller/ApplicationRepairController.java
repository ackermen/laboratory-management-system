package group.teachingmanagerbk.controller;

import group.teachingmanagerbk.service.ApplicationRepairService;
import group.teachingmanagerbk.utils.ReturnResult.Result;
import group.teachingmanagerbk.vo.repair.LabRepair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ApplicationRepairController {

    @Autowired
    ApplicationRepairService applicationRepairService;


    @PostMapping("/apply/add/repair")
    public Result applyNewRepair(@RequestBody LabRepair labRepair){
        applicationRepairService.applyNewRepair(labRepair);
        return new Result().success();
    }

    @GetMapping("/repair/application")
    public Result getRepairApplicationByTeacherId(Integer teacherId){
        ArrayList<LabRepair> labRepairs;
        labRepairs = applicationRepairService.getRepairApplicationByTeacherId(teacherId);
        return new Result().success(labRepairs);
    }

    @GetMapping("/repair/get/application")
    public Result getApplicationByTeacherIdAndRepairStatusName(Integer teacherId,String repairStatusName){
        ArrayList<LabRepair> labRepairs;
        labRepairs = applicationRepairService.getApplicationByTeacherIdAndRepairStatusName(teacherId,repairStatusName);
        return new Result().success(labRepairs);
    }

}
