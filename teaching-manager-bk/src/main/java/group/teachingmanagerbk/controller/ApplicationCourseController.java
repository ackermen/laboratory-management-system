package group.teachingmanagerbk.controller;

import group.teachingmanagerbk.dto.application.ApplicationCourse;
import group.teachingmanagerbk.dto.application.CourseApplication;
import group.teachingmanagerbk.mapper.ApplicationCourseMapper;
import group.teachingmanagerbk.service.ApplicationCourseService;
import group.teachingmanagerbk.service.TermService;
import group.teachingmanagerbk.utils.ReturnResult.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class ApplicationCourseController {

    @Autowired
    ApplicationCourseService applicationCourseService;



    @PostMapping("/apply/add/course")
    public Result applyNewCourse(@RequestBody ApplicationCourse course){
        Result result = new Result();
        if(course.getStartWeek() > course.getEndWeek()){

            return result.error("申请的起始周和结束周不合理。");
        }
        applicationCourseService.applyNewCourse(course);
        return result.success();
    }

    @GetMapping("/get/application")
    public Result getApplicationCourseId(Long courseApplicationId){
        ApplicationCourse data = applicationCourseService.getCourseApplicationById(courseApplicationId);
        return new Result().success(data);
    }

    @PostMapping("/update/application")
    public Result updateApplication(@RequestBody ApplicationCourse applicationCourse){
        applicationCourseService.updateApplication(applicationCourse);
        return new Result().success();
    }
}
