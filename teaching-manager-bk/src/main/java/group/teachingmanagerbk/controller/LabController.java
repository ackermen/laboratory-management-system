package group.teachingmanagerbk.controller;

import group.teachingmanagerbk.dto.lab.Laboratory;
import group.teachingmanagerbk.service.LabService;
import group.teachingmanagerbk.utils.ReturnResult.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@Slf4j
@RestController
public class LabController {
    @Autowired
    LabService labService;

    @GetMapping("/Lab/getAll")
    public Result getAllLab(){
        ArrayList<Laboratory> laboratories =  labService.getAllLab();
        return new Result().success(laboratories);
    }
}
