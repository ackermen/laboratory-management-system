package group.teachingmanagerbk.controller;

import group.teachingmanagerbk.dto.lab.LabSchedules;
import group.teachingmanagerbk.service.LabSchedulesService;
import group.teachingmanagerbk.utils.ReturnResult.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@Slf4j
@RestController
public class LabSchedulesController {
    @Autowired
    LabSchedulesService labSchedulesService;


    @GetMapping("/course/labSchedules/{year}")
    public Result getLabSchedules(@PathVariable String year){
        ArrayList<LabSchedules> labSchedulesArrayList = labSchedulesService.getLabSchedulesByYear(year);

        return new Result().success(labSchedulesArrayList);
    }
}
