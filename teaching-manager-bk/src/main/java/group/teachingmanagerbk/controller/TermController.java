package group.teachingmanagerbk.controller;

import group.teachingmanagerbk.service.TermService;
import group.teachingmanagerbk.utils.ReturnResult.Result;
import group.teachingmanagerbk.vo.course.Term;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@Slf4j
@RestController
public class TermController {
    @Autowired
    TermService termService;

    //获取学期信息
    @GetMapping("/get/terms")
    public Result getTeachers() {
        ArrayList<Term> terms = termService.getTerms();
        return new Result().success(terms);
    }

    //获取当前学期信息
    @GetMapping("/get/currentTerm")
    public String getCurrentTerm() {
        return termService.getCurrentTerm();
    }

    //更新当前学期信息
    @PutMapping("/update/currentTerm")
    public void updateCurrentTerm(String term) {
        termService.updateCurrentTerm(term);
    }

    //新增学期信息
    @PostMapping("/term")
    public Result addTermInfo(@RequestBody Term json) {
        if (termService.insertTerm(json)) {
            return new Result().success();
        }
        return new Result().error("插入数据失败！");
    }

}
