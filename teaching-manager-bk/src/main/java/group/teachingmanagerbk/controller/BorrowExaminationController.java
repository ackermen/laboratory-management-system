package group.teachingmanagerbk.controller;


import group.teachingmanagerbk.service.BorrowExaminationService;
import group.teachingmanagerbk.utils.ReturnResult.Result;
import group.teachingmanagerbk.vo.Lab.StudentLab;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class BorrowExaminationController {

    @Autowired
    BorrowExaminationService borrowExaminationService;

    //查询所有审批的数据
    @GetMapping("/borrow/examination")
    public Result getWaitExamination() {
        ArrayList<StudentLab> data = borrowExaminationService.getBorrowExamination();
        return new Result().success(data);
    }


    //通过审批一条学生记录
    @PutMapping("/borrow/examination/agree")
    public Result agreeABorrow(int labBorrowingId) {
        try {
            borrowExaminationService.agreeABorrow(labBorrowingId);
            return new Result().success();
        } catch (Exception e) {
            return new Result().error(e.getMessage());
        }
    }

    //不通过审批一条学生记录
    @PutMapping("/borrow/examination/reject")
    public Result rejectABorrow(int labBorrowingId) {
        try {
            borrowExaminationService.rejectABorrow(labBorrowingId);
            return new Result().success();
        } catch (Exception e) {
            return new Result().error(e.getMessage());
        }
    }

}
