package group.teachingmanagerbk.controller;

import group.teachingmanagerbk.dto.member.QueryStudentParam;
import group.teachingmanagerbk.dto.member.QueryTeacherParam;
import group.teachingmanagerbk.dto.member.QueryTesterParam;
import group.teachingmanagerbk.listener.SysUserImportListener;
import group.teachingmanagerbk.listener.TeacherImportListener;
import group.teachingmanagerbk.listener.TesterImportListener;
import group.teachingmanagerbk.service.LabService;
import group.teachingmanagerbk.service.MemberService;
import group.teachingmanagerbk.utils.ReturnResult.R;
import group.teachingmanagerbk.utils.ReturnResult.Result;
import group.teachingmanagerbk.utils.ReturnResult.ResultWithTotal;
import group.teachingmanagerbk.utils.excel.ExcelResult;
import group.teachingmanagerbk.utils.excel.poi.ExcelUtil;
import group.teachingmanagerbk.vo.Lab.StudentLab;
import group.teachingmanagerbk.vo.member.*;
import  jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;

@Slf4j
@RestController
public class MemberController {

    @Autowired
    MemberService memberService;
    @Autowired
    LabService labService;
    //获取学院的信息
   /* @GetMapping("/departments")
    public Result getDepartmentsInfo() {
        return new Result().success(memberService.getDepartmentInfo());
    }*/

    //获取教师信息
    @PostMapping("/teachers")
    public Result getTeachersInfo(@RequestBody QueryTeacherParam json) {
        log.info(json.toString());
        ResultWithTotal teachersInfo = memberService.getTeachersInfo(json);
        return teachersInfo.success();
    }

    //获取教师信息
    @GetMapping("/get/teachers")
    public Result getTeachers() {
        ArrayList<Teacher> teachers = memberService.getTeachers();
        return new Result().success(teachers);
    }

    //新增教师信息
    @PostMapping("/teacher")
    public Result addTeacherInfo(@RequestBody Teacher json) {
        if (memberService.insertTeacher(json)) {
            return new Result().success();
        }
        return new Result().error("插入数据失败！");
    }

    //根据id删除教师的信息
    @DeleteMapping("/teachers/{teacherIds}")
    public Result deleteTeachersInfo(@PathVariable String[] teacherIds) {
        log.info("根据id{}删除教师的信息", (Object) teacherIds);
        memberService.deleteTeacher(teacherIds);
        return new Result().success();
    }

    //根据教师id查询对应的数据
    @GetMapping("/get/teacher")
    public Result getTeacherInfoById(@RequestParam String teacherId) {
        Teacher info = memberService.getTeacherInfoById(teacherId);
        return new Result().success(info);
    }

    //根据教师id修改教师信息
    @PutMapping("/modify/teacher")
    public Result modifyTeacherInfo(@RequestBody Teacher teacher) {
        log.info(teacher.toString());
        if (memberService.updateTeacherInfoById(teacher)) {
            return new Result().success();
        };
        return new Result().error("更新信息失败！");
    }

    //获取学生信息
    @PostMapping("/students")
    public Result getStudentsInfo(@RequestBody QueryStudentParam json) {
        log.info(json.toString());
        ResultWithTotal studentsInfo = memberService.getStudentsInfo(json);
        return studentsInfo.success();
    }

    //新增学生信息
    @PostMapping("/student")
    public Result addStudentInfo(@RequestBody Student json) {
        if (memberService.insertStudent(json)) {
            return new Result().success();
        }
        return new Result().error("插入数据失败！");
    }

    //根据id删除学生的信息
    @DeleteMapping("/students/{studentIds}")
    public Result deleteStudentsInfo(@PathVariable String[] studentIds) {
        log.info("根据id{}删除学生的信息", (Object) studentIds);
        memberService.deleteStudent(studentIds);
        return new Result().success();
    }

    //根据学生id查询对应的数据
    @GetMapping("/get/student")
    public Result getStudentInfoById(@RequestParam String studentId) {
        Student info = memberService.getStudentInfoById(studentId);
        return new Result().success(info);
    }

    //根据学生id修改学生信息
    @PutMapping("/modify/student")
    public Result modifyStudentInfo(@RequestBody Student student) {
        log.info(student.toString());
        if (memberService.updateStudentInfoById(student)) {
            return new Result().success();
        };
        return new Result().error("更新信息失败！");
    }

    //获取Tester信息
    @PostMapping("/testers")
    public Result getTestersInfo(@RequestBody QueryTesterParam json) {
        log.info(json.toString());
        ResultWithTotal testersInfo = memberService.getTestersInfo(json);
        return testersInfo.success();
    }

    //新增Tester信息
    @PostMapping("/tester")
    public Result addTesterInfo(@RequestBody Tester json) {
        if (memberService.insertTester(json)) {
            return new Result().success();
        }
        return new Result().error("插入数据失败！");
    }

    //根据id删除Tester的信息
    @DeleteMapping("/testers/{testerIds}")
    public Result deleteTestersInfo(@PathVariable String[] testerIds) {
        log.info("根据id{}删除Tester的信息", (Object) testerIds);
        memberService.deleteTester(testerIds);
        return new Result().success();
    }

    //根据Tester id查询对应的数据
    @GetMapping("/get/tester")
    public Result getTesterInfoById(@RequestParam String testerId) {
        Tester info = memberService.getTesterInfoById(testerId);
        return new Result().success(info);
    }

    //根据Tester id修改Tester信息
    @PutMapping("/modify/tester")
    public Result modifyTesterInfo(@RequestBody Tester tester) {
        log.info(tester.toString());
        if (memberService.updateTesterInfoById(tester)) {
            return new Result().success();
        };
        return new Result().error("更新信息失败！");
    }



    /**
     * 批量导入学生数据
     *
     * @param file          导入文件
     * @param updateSupport 是否更新已存在数据
     */
    @PostMapping(value = "/student/importData", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public R<Void> importStudentData(@RequestPart("file") MultipartFile file, boolean updateSupport) throws Exception {
        ExcelResult<SysUserImportVo> result = ExcelUtil.importExcel(file.getInputStream(), SysUserImportVo.class, new SysUserImportListener(updateSupport));
        return R.ok(result.getAnalysis());
    }

    /**
     * 获取学生导入模板
     */
    @PostMapping("/student/importTemplate")
    public void importStudentTemplate(HttpServletResponse response) {
        ExcelUtil.exportExcel(new ArrayList<>(), "学生批量导入模板", SysUserImportVo.class, response);
    }

    /**
     * 批量导入教师数据
     *
     * @param file          导入文件
     * @param updateSupport 是否更新已存在数据
     */
    @PostMapping(value = "/teacher/importData", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public R<Void> importTeacherData(@RequestPart("file") MultipartFile file, boolean updateSupport) throws Exception {
        ExcelResult<TeacherImportVo> result = ExcelUtil.importExcel(file.getInputStream(), TeacherImportVo.class, new TeacherImportListener(updateSupport));
        return R.ok(result.getAnalysis());
    }

    /**
     * 获取教师导入模板
     */
    @PostMapping("/teacher/importTemplate")
    public void importTeacherTemplate(HttpServletResponse response) {
        ExcelUtil.exportExcel(new ArrayList<>(), "教师批量导入模板", TeacherImportVo.class, response);
    }

    /**
     * 批量导入Tester数据
     *
     * @param file          导入文件
     * @param updateSupport 是否更新已存在数据
     */
    @PostMapping(value = "/tester/importData", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public R<Void> importTesterData(@RequestPart("file") MultipartFile file, boolean updateSupport) throws Exception {
        ExcelResult<TesterImportVo> result = ExcelUtil.importExcel(file.getInputStream(), TesterImportVo.class, new TesterImportListener(updateSupport));
        return R.ok(result.getAnalysis());
    }

    /**
     * 获取Tester导入模板
     */
    @PostMapping("/tester/importTemplate")
    public void importTesterTemplate(HttpServletResponse response) {
        ExcelUtil.exportExcel(new ArrayList<>(), "实验员批量导入模板", TesterImportVo.class, response);
    }
    /*******************申请实验室************************/

    /**
     * @author: SCAU_CWJ
     * @date: 2024/4/5
     * @description: 申请实验室
     */
    @GetMapping("/student/select/lab")
    public R getLab(@RequestParam("studentId") int id)
    {
        System.out.println("申请实验室测试.....");
        /**
         * @author: SCAU_CWJ
         * @date: 2024/4/5
         * @description: 端口测试成功
         */
        return R.ok(labService.getLabInfo(id));
    }
    /**
     * @author: SCAU_CWJ
     * @date: 2024/4/7
     * @description:学生新增/更新申请实验室信息
     */
    @PostMapping("student/lab/update")
    public R updateLab(@RequestBody StudentLab lab)
    {
        System.out.println(lab);
        return R.ok(labService.update(lab));
    }
    /**
     * @author: SCAU_CWJ
     * @date: 2024/4/8
     * @description:新增lab申请
     */
    @PostMapping("student/lab/add")
    public R addLab(@RequestBody StudentLab lab)
    {
        System.out.println("测试学生名是否存在   ："+lab);
        return R.ok(labService.add(lab));
    }

    /**
     * @author: SCAU_CWJ
     * @date: 2024/4/12
     * @description:实验室使用完毕
     */
    @PostMapping("student/lab/accept")
    public R acceptLab(@RequestBody StudentLab lab)
    {
        System.out.println("dwa");
        System.out.println(lab.getLabBorrowingId());
        return R.ok(labService.accpet( lab.getLabBorrowingId()));
    }
}
