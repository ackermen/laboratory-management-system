package group.teachingmanagerbk.service;

import group.teachingmanagerbk.vo.repair.LabRepair;

import java.util.ArrayList;

public interface RepairService {

    //获取所有维修记录
    ArrayList<LabRepair> getLabRepair(String currentTerm, String curId);

    //开始一个维修
    void startRepair(int equipmentRepairId) throws Exception;

    //完成一个维修
    void finishRepair(int repairStateId, String descriptionOfRepair, int equipmentRepairId) throws Exception;

    LabRepair getRepairById(Integer repairId);
}
