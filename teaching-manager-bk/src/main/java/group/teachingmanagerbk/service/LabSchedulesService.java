package group.teachingmanagerbk.service;

import group.teachingmanagerbk.dto.lab.LabSchedules;

import java.util.ArrayList;

public interface LabSchedulesService {
    ArrayList<LabSchedules> getLabSchedulesByYear(String year);
}
