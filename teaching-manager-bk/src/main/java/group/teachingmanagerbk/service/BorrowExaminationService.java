package group.teachingmanagerbk.service;

import group.teachingmanagerbk.dto.application.ApplicationCourse;
import group.teachingmanagerbk.dto.application.CourseApplication;
import group.teachingmanagerbk.vo.Lab.StudentLab;

import java.util.ArrayList;

public interface BorrowExaminationService {

    /**
     * 查询审批的所有申请记录
     * @return 申请记录
     */
    ArrayList<StudentLab> getBorrowExamination();

    /**
     * 不通过审批一条申请记录
     */
    void rejectABorrow(int labBorrowingId) throws Exception;
    /**
     * 通过审批一条申请记录
     */
    void agreeABorrow(int labBorrowingId) throws Exception;

}
