package group.teachingmanagerbk.service;

import group.teachingmanagerbk.dto.lab.Laboratory;

import java.util.ArrayList;
import group.teachingmanagerbk.vo.Lab.StudentLab;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
public interface LabService {
    ArrayList<Laboratory> getAllLab();

    /**
     * @author: SCAU_CWJ
     * @date: 2024/4/5
     * @description: 根据学生id查询他的申请列表
     */
    public List<StudentLab> getLabInfo(int id);
    public boolean update(StudentLab lab);
    public boolean add(StudentLab Lab);
    public boolean accpet(int id);
}
