package group.teachingmanagerbk.service.impl;

import group.teachingmanagerbk.dto.lab.LabSchedules;
import group.teachingmanagerbk.mapper.LabSchedulesMapper;
import group.teachingmanagerbk.service.LabSchedulesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Slf4j
@Service
public class LabSchedulesServiceImpl implements LabSchedulesService {
    @Autowired
    LabSchedulesMapper labSchedulesMapper;

    @Override
    public ArrayList<LabSchedules> getLabSchedulesByYear(String year) {
        ArrayList<LabSchedules> labSchedulesArrayList = labSchedulesMapper.getLabSchedulesByYear(year);
        return labSchedulesArrayList;
    }
}
