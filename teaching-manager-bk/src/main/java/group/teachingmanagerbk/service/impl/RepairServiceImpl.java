package group.teachingmanagerbk.service.impl;

import group.teachingmanagerbk.mapper.RepairMapper;
import group.teachingmanagerbk.service.RepairService;
import group.teachingmanagerbk.vo.repair.LabRepair;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Slf4j
@Service
public class RepairServiceImpl implements RepairService{

    @Autowired
    RepairMapper repairMapper;

    @Override
    public ArrayList<LabRepair> getLabRepair(String currentTerm, String curId) {
        return repairMapper.getLabRepair(currentTerm, curId);
    }

    @Override
    public void startRepair(int equipmentRepairId) {
        repairMapper.startRepair(2, equipmentRepairId);
    }

    @Override
    public void finishRepair(int repairStateId, String descriptionOfRepair, int equipmentRepairId) {
        repairMapper.finishRepair(descriptionOfRepair, repairStateId, equipmentRepairId);
    }

    @Override
    public LabRepair getRepairById(Integer repairId) {

        return repairMapper.getRepairById(repairId);
    }
}
