package group.teachingmanagerbk.service.impl;

import group.teachingmanagerbk.dto.application.ApplicationCourse;
import group.teachingmanagerbk.mapper.ApplicationCourseMapper;
import group.teachingmanagerbk.mapper.TermMapper;
import group.teachingmanagerbk.service.ApplicationCourseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ApplicationCourseServiceImpl implements ApplicationCourseService {

    @Autowired
    ApplicationCourseMapper applicationCourseMapper;

    @Autowired
    TermMapper termMapper;

    /**
     * 申请新的课程
     * @param course
     */
    @Override
    public void applyNewCourse(ApplicationCourse course) {
        course.setOperationId(1L);
        course.setCourseExaminationId(1L);
        applicationCourseMapper.insertNewCourse(course);
    }

    @Override
    public ApplicationCourse getCourseApplicationById(Long courseApplicationId) {
        String year = termMapper.getCurrentTerm();
        return applicationCourseMapper.getCourseApplicationById(courseApplicationId,year);
    }

    @Override
    public void updateApplication(ApplicationCourse applicationCourse) {
        applicationCourseMapper.updateApplication(applicationCourse);
    }
}
