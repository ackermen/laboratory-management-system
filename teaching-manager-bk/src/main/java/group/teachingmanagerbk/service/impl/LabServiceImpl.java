package group.teachingmanagerbk.service.impl;

import group.teachingmanagerbk.dto.lab.Laboratory;
import group.teachingmanagerbk.mapper.LabMapper;
import group.teachingmanagerbk.mapper.TermMapper;
import group.teachingmanagerbk.service.LabService;
import group.teachingmanagerbk.utils.ReturnResult.R;
import group.teachingmanagerbk.vo.Lab.StudentLab;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service

public class LabServiceImpl implements LabService {
    @Autowired
    LabMapper labMapper;

    @Autowired
    TermMapper termMapper;
    @Override
    public List<StudentLab> getLabInfo(int id) {
        return labMapper.getStudentInfo(id);
    }

    @Override
    public ArrayList<Laboratory> getAllLab() {
        ArrayList<Laboratory> laboratories = labMapper.getLab();
        return laboratories;

    }

    @Override
    public boolean update(StudentLab lab) {
        lab.setYear(termMapper.getCurrentTerm());
        return labMapper.update(lab);
    }

    @Override
    public boolean add(StudentLab lab) {
        lab.setYear(termMapper.getCurrentTerm());
        return labMapper.add(lab);
    }





    @Override
    public boolean accpet(int labBorrowingId) {
        return labMapper.accpet(labBorrowingId);
    }


}
