package group.teachingmanagerbk.service.impl;

import group.teachingmanagerbk.dto.application.ApplicationCourse;
import group.teachingmanagerbk.dto.application.CourseApplication;
import group.teachingmanagerbk.dto.lab.LabSchedules;
import group.teachingmanagerbk.dto.lab.Laboratory;
import group.teachingmanagerbk.mapper.*;
import group.teachingmanagerbk.service.CourseExaminationService;
import group.teachingmanagerbk.service.LabSchedulesService;
import group.teachingmanagerbk.vo.course.Course;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Slf4j
@Service
public class CourseExaminationServiceImpl  implements CourseExaminationService {

    @Autowired
    CourseExaminationMapper courseExaminationMapper;
    @Autowired
    CourseMapper courseMapper;

    @Autowired
    ApplicationCourseMapper applicationCourseMapper;

    @Autowired
    LabMapper labMapper;

    @Autowired
    LabSchedulesMapper labSchedulesMapper;

    @Autowired
    TermMapper termMapper;

    @Override
    public ArrayList<CourseApplication> getWaitExamination(String examinationName) {
        String year = termMapper.getCurrentTerm();
        return courseExaminationMapper.getWaitExamination(examinationName,year);
    }

    @Override
    public ArrayList<CourseApplication> getAlreadyExamination(String examinationName) {
        String year = termMapper.getCurrentTerm();
        return courseExaminationMapper.getAlreadyExamination(examinationName,year);
    }

    @Override
    public void examineACourse(ApplicationCourse course) throws Exception {
        //首先判断通过还是不通过
        String courseExaminationName = course.getCourseExaminationName();
        Long courseApplicationId = course.getCourseApplicationId();
        Long courseExaminationId = course.getCourseExaminationId();
        if ("未通过".equals(courseExaminationName)) {
            //更新检查状态
            courseExaminationId = 3L;
            courseExaminationMapper.updateCourseApplicationExamination(courseExaminationId, courseApplicationId);
            return;
        }
        //判断是否是合法的检查名称
        if (courseExaminationId == null) {
            throw new Exception("查询检查状态id错误，请检查前端传过来的检查名称是否正确！");
        }
        //接下来是通过之后的操作
        String operationName = course.getOperationName();

        switch (operationName) {
            case "新增" -> {
                String labType = course.getLabType();
                ArrayList<Laboratory> laboratories = labMapper.getLabByLabType(labType);
                ArrayList<LabSchedules> labSchedules;
                int judge1 = 0;//可用课室判断
                int judge2 = 0;//人数判断
                int maxNum = 0;//最大人数
                LabSchedules l = new LabSchedules();
                for (Laboratory laboratory : laboratories) {
                    course.setLabNum(laboratory.getLabNum());
                    labSchedules = labSchedulesMapper.judge(course);
                    if(labSchedules == null || labSchedules.size() == 0){
                        System.out.println("出现");
                        if(laboratory.getNumber() < course.getStudentNum()){
                            judge2 = 1;
                            maxNum = laboratory.getNumber() > maxNum ? laboratory.getNumber() : maxNum;
                            continue;
                        }
                        judge1 = 1;
                        BeanUtils.copyProperties(course,l);
                        String weeks = course.getStartWeek() + "-" + course.getEndWeek();
                        l.setWeeks(weeks);
                        l.setLabName(laboratory.getLabName());
                        labSchedulesMapper.insertNewSchedule(l);
                        applicationCourseMapper.updateLabNum(courseApplicationId,course.getLabNum());
                        break;
                    }
                }
                if(judge1 == 0 && judge2 == 1){
                    courseExaminationMapper.updateCourseApplicationExamination( 3L, courseApplicationId);
                    throw new Exception("审核不通过！原因：人数超出可选课室容量!最大容量为" + maxNum + "人");
                }else if (judge1 == 0 && judge2 == 0){
                    courseExaminationMapper.updateCourseApplicationExamination( 3L, courseApplicationId);
                    throw new Exception("审核不通过！原因：无对应可选课室!");
                }

               /* Course newCourse = buildANewCourseInfo(course);
                courseMapper.insertCourse(newCourse);
                log.info(newCourse.getCourseId());*/
                courseExaminationMapper.updateCourseApplicationExamination( 2L, courseApplicationId);


            }
            case "修改" -> {
                /*Course modifiedCourse = buildModifiedCourseInfo(course);
                courseMapper.modifyCourse(modifiedCourse);
                courseExaminationMapper.updateCourseApplicationExamination(courseExaminationId, courseApplicationId);*/
            }
            /*case "删除" -> courseMapper.deleteCourseByIds(new String[]{course.getCourseId()});*/
        }
    }

    //根据申请构建一个新的课程信息
    private Course buildANewCourseInfo(CourseApplication courseApplication) throws Exception {
        Course course = new Course();
        //根据名称获取课程状态id
        String courseStatusId = courseMapper.getCourseStatusIdByName("等待课程安排");
        if (courseStatusId == null) {
            throw new Exception("后端错误：所确定的课程状态在数据库中不存在，后端请修改！");
        }
        buildCommonData(course, courseApplication);
        course.setCourseStatusId(courseStatusId);
        return course;
    }

    //根据申请修改一个课程信息
    private Course buildModifiedCourseInfo(CourseApplication courseApplication) {
        Course course = new Course();
        buildCommonData(course, courseApplication);
        String courseId = courseApplication.getCourseId();
        course.setCourseId(courseId);
        return course;
    }

    //构建新建课程与修改课程之间相同部分的数据
    private void buildCommonData(Course course, CourseApplication courseApplication) {
        //读取申请里面的信息
        String name = courseApplication.getCourseName();
        String teacherId = courseApplication.getTeacherId();
        String credit = courseApplication.getCourseCredit();
        String hour = courseApplication.getCourseHour();
        String time = courseApplication.getCourseTime();
        String placeId = courseApplication.getCoursePlaceId();
        String description = courseApplication.getCourseDescription();
        //设置课程信息
        course.setName(name);
        course.setTeacherId(teacherId);
        course.setCredit(credit);
        course.setHour(hour);
        course.setTime(time);
        course.setPlaceId(placeId);
        course.setDescription(description);
    }

}
