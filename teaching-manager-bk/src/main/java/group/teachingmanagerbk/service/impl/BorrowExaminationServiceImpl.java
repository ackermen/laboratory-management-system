package group.teachingmanagerbk.service.impl;

import group.teachingmanagerbk.mapper.*;
import group.teachingmanagerbk.service.BorrowExaminationService;
import group.teachingmanagerbk.vo.Lab.StudentLab;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Slf4j
@Service
public class BorrowExaminationServiceImpl implements BorrowExaminationService {

    @Autowired
    BorrowExaminationMapper borrowExaminationMapper;

    @Autowired
    TermMapper termMapper;

    @Override
    public ArrayList<StudentLab> getBorrowExamination() {
        String year = termMapper.getCurrentTerm();
        return borrowExaminationMapper.getBorrowExamination(year);
    }

    @Override
    public void rejectABorrow(int labBorrowingId) {
        borrowExaminationMapper.rejectBorrowExamination("不通过",labBorrowingId);
    }
    @Override
    public void agreeABorrow(int labBorrowingId) {
        borrowExaminationMapper.agreeBorrowExamination("通过",labBorrowingId);
    }

}
