package group.teachingmanagerbk.service.impl;

import group.teachingmanagerbk.dto.application.CourseApplication;
import group.teachingmanagerbk.mapper.CourseApplicationMapper;
import group.teachingmanagerbk.mapper.TermMapper;
import group.teachingmanagerbk.service.CourseApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class CourseApplicationServiceImpl implements CourseApplicationService {

    @Autowired
    CourseApplicationMapper courseApplicationMapper;

    @Autowired
    TermMapper termMapper;

    @Override
    public boolean applyANewCourse(CourseApplication json) {
        String operationName = json.getOperationName();
        String operationId = courseApplicationMapper.getOperationIdByName(operationName);
        String courseExaminationName = json.getCourseExaminationName();
        String examinationId = courseApplicationMapper.getExaminationIdByName(courseExaminationName);
        if (operationId == null || examinationId == null) {
            return false;
        }
        json.setOperationId(operationId);
        json.setCourseExaminationId(examinationId);
        courseApplicationMapper.insertANewApplication(json);
        return true;
    }

    @Override
    public ArrayList<CourseApplication> getAllApplicationByTeacherId(String teacherId) {
        String year = termMapper.getCurrentTerm();
        return courseApplicationMapper.getAllApplicationByTeacherId(teacherId,year);
    }

    @Override
    public ArrayList<CourseApplication> getApplicationByTeacherIdAndExaminationName(String teacherId, String courseExaminationName) {
        String year = termMapper.getCurrentTerm();
        return courseApplicationMapper.getApplicationByTeacherIdAndExaminationName(teacherId, courseExaminationName,year);
    }

    @Override
    public CourseApplication getCourseApplicationById(String courseApplicationId) {
        return courseApplicationMapper.getCourseApplicationById(courseApplicationId);
    }

}
