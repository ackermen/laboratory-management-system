package group.teachingmanagerbk.service.impl;

import group.teachingmanagerbk.dto.member.QueryStudentParam;
import group.teachingmanagerbk.dto.member.QueryTeacherParam;
import group.teachingmanagerbk.dto.member.QueryTesterParam;
import group.teachingmanagerbk.mapper.MemberMapper;
import group.teachingmanagerbk.service.MemberService;
import group.teachingmanagerbk.utils.ReturnResult.ResultWithTotal;
import group.teachingmanagerbk.vo.member.Department;
import group.teachingmanagerbk.vo.member.Student;
import group.teachingmanagerbk.vo.member.Teacher;
import group.teachingmanagerbk.vo.member.Tester;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Slf4j
@Service
public class MemberServiceImpl implements MemberService {

    @Autowired
    MemberMapper memberMapper;

   /* @Override
    public ArrayList<Department> getDepartmentInfo() {
        return memberMapper.getDepartmentInfo();
    }*/

    @Override
    public ResultWithTotal getTeachersInfo(QueryTeacherParam json) {
        Teacher param = json.getParam();
        Integer pageSize = json.getPageSize();
        Integer currentPage = json.getCurrentPage();
        Integer index = pageSize * (currentPage - 1);
        int total = memberMapper.getTeacherCount(param);
        ArrayList<Teacher> teacherInfo = memberMapper.getTeacherInfo(param, index, pageSize);
        ResultWithTotal result = new ResultWithTotal();
        result.setTotal(total);
        result.setData(teacherInfo);
        return result;
    }

    @Override
    public ArrayList<Teacher> getTeachers() {
        return memberMapper.getTeachers();
    }

    @Override
    public boolean insertTeacher(Teacher json) {
        //需要先判断json中的数据是否为空
        if (checkTeacherBlank(json)) return false;
        memberMapper.insertTeacherInfo(json);
        return true;
    }

    @Override
    public void deleteTeacher(String[] teacherIds) {
        memberMapper.deleteTeachersInfo(teacherIds);
    }

    @Override
    public Teacher getTeacherInfoById(String teacherId) {
        return memberMapper.getTeacherInfoById(teacherId);
    }

    @Override
    public Teacher getTeacherInfoByNumber(String number) {
        return memberMapper.getTeacherInfoByNumber(number);
    }

    @Override
    public boolean updateTeacherInfoByNumber(Teacher teacher) {
        //需要先判断student中的数据是否为空
        if (checkTeacherBlank(teacher)) return false;
        memberMapper.updateTeacherInfoByNumber(teacher);
        return true;
    }

    @Override
    public boolean updateTeacherInfoById(Teacher teacher) {
        //需要先判断student中的数据是否为空
        if (checkTeacherBlank(teacher)) return false;
        memberMapper.updateTeacherInfoById(teacher);
        return true;
    }

    @Override
    public ResultWithTotal getStudentsInfo(QueryStudentParam json) {
        Student param = json.getParam();
        Integer pageSize = json.getPageSize();
        Integer currentPage = json.getCurrentPage();
        Integer index = pageSize * (currentPage - 1);
        int total = memberMapper.getStudentCount(param);
        ArrayList<Student> studentInfo = memberMapper.getStudentInfo(param, index, pageSize);
        ResultWithTotal result = new ResultWithTotal();
        result.setTotal(total);
        result.setData(studentInfo);
        return result;
    }

    @Override
    public boolean insertStudent(Student json) {
        //需要先判断json中的数据是否为空
        if (checkStudentBlank(json)) return false;
        memberMapper.insertStudentInfo(json);
        return true;
    }

    @Override
    public void deleteStudent(String[] studentIds) {
        memberMapper.deleteStudentsInfo(studentIds);
    }

    @Override
    public Student getStudentInfoById(String studentId) {
        return memberMapper.getStudentInfoById(studentId);
    }

    @Override
    public Student getStudentInfoByName(String name) {
        return memberMapper.getStudentInfoByName(name);
    }

    @Override
    public Student getStudentInfoByNumber(String number) {
        return memberMapper.getStudentInfoByNumber(number);
    }

    @Override
    public boolean updateStudentInfoByNumber(Student student) {
        //需要先判断student中的数据是否为空
        if (checkStudentBlank(student)) return false;
        memberMapper.updateStudentInfoByNumber(student);
        return true;
    }

    @Override
    public boolean updateStudentInfoById(Student student) {
        //需要先判断student中的数据是否为空
        if (checkStudentBlank(student)) return false;
        memberMapper.updateStudentInfoById(student);
        return true;
    }

    @Override
    public boolean updateStudentInfoByName(Student student) {
        //需要先判断student中的数据是否为空
        if (checkStudentBlank(student)) return false;
        memberMapper.updateStudentInfoByName(student);
        return true;
    }

    @Override
    public ResultWithTotal getTestersInfo(QueryTesterParam json) {
        Tester param = json.getParam();
        Integer pageSize = json.getPageSize();
        Integer currentPage = json.getCurrentPage();
        Integer index = pageSize * (currentPage - 1);
        int total = memberMapper.getTesterCount(param);
        ArrayList<Tester> testerInfo = memberMapper.getTesterInfo(param, index, pageSize);
        ResultWithTotal result = new ResultWithTotal();
        result.setTotal(total);
        result.setData(testerInfo);
        return result;
    }

    @Override
    public boolean insertTester(Tester json) {
        //需要先判断json中的数据是否为空
        if (checkTesterBlank(json)) return false;
        memberMapper.insertTesterInfo(json);
        return true;
    }

    @Override
    public void deleteTester(String[] TesterIds) {
        memberMapper.deleteTestersInfo(TesterIds);
    }

    @Override
    public Tester getTesterInfoById(String TesterId) {
        return memberMapper.getTesterInfoById(TesterId);
    }

    @Override
    public Tester getTesterInfoByNumber(String number) {
        return memberMapper.getTesterInfoByNumber(number);
    }

    @Override
    public boolean updateTesterInfoByNumber(Tester tester) {
        //需要先判断student中的数据是否为空
        if (checkTesterBlank(tester)) return false;
        memberMapper.updateTesterInfoByNumber(tester);
        return true;
    }

    @Override
    public boolean updateTesterInfoById(Tester tester) {
        //需要先判断tester中的数据是否为空
        if (checkTesterBlank(tester)) return false;
        memberMapper.updateTesterInfoById(tester);
        return true;
    }


    private boolean checkStudentBlank(Student student) {
        String studentNumber = student.getStudentNumber();
        String str1 = studentNumber.replaceAll(" ", "");
        String name = student.getName();
        String str2 = name.replaceAll(" ", "");
        String studentClass = student.getStudentClass();
        String str3 = studentClass.replaceAll(" ", "");
        return "".equals(str1) || "".equals(str2) || "".equals(str3);
    }

    private  boolean checkTeacherBlank(Teacher teacher) {
        String teacherNumber = teacher.getTeacherNumber();
        String str1 = teacherNumber.replaceAll(" ","");
        String name = teacher.getName();
        String str2 = name.replaceAll(" ","");
        /*String departmentName = teacher.getDepartmentName();
        String str3 = departmentName.replaceAll(" ","");
        String departmentId = teacher.getDepartmentId();
        String str4 = departmentId.replaceAll(" ", "");*/
        return "".equals(str1) || "".equals(str2) /*|| "".equals(str3) || "".equals(str4)*/;
    }

    private  boolean checkTesterBlank(Tester tester) {
        String testerNumber = tester.getTesterNumber();
        String str1 = testerNumber.replaceAll(" ","");
        String name = tester.getName();
        String str2 = name.replaceAll(" ","");
        return "".equals(str1) || "".equals(str2);
    }

}
