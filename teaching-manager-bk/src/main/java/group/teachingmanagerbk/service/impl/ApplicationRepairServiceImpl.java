package group.teachingmanagerbk.service.impl;

import group.teachingmanagerbk.mapper.ApplicationRepairMapper;
import group.teachingmanagerbk.mapper.LabMapper;
import group.teachingmanagerbk.mapper.TermMapper;
import group.teachingmanagerbk.service.ApplicationRepairService;
import group.teachingmanagerbk.vo.repair.LabRepair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ApplicationRepairServiceImpl implements ApplicationRepairService {

    @Autowired
    LabMapper labMapper;

    @Autowired
    ApplicationRepairMapper applicationRepairMapper;

    @Autowired
    TermMapper termMapper;

    @Override
    public void applyNewRepair(LabRepair labRepair) {
        labRepair.setTesterId(labMapper.getTesterIdByLabNum(labRepair.getLabNum()));
        labRepair.setRepairStatusId(1);
        applicationRepairMapper.addNewRepair(labRepair);
    }

    @Override
    public ArrayList<LabRepair> getRepairApplicationByTeacherId(Integer teacherId) {
        String year = termMapper.getCurrentTerm();
        return applicationRepairMapper.getRepairApplicationByTeacherId(teacherId,year);
    }

    @Override
    public ArrayList<LabRepair> getApplicationByTeacherIdAndRepairStatusName(Integer teacherId, String repairStatusName) {
        String year = termMapper.getCurrentTerm();
        Integer repairStatusId = labMapper.getRepairStatusIdByStatusName(repairStatusName);
        return applicationRepairMapper.getApplicationByTeacherIdAndRepairStatusName(teacherId,repairStatusId,year);
    }
}
