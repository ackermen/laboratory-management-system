package group.teachingmanagerbk.service.impl;

import group.teachingmanagerbk.dto.member.QueryStudentParam;
import group.teachingmanagerbk.dto.member.QueryTeacherParam;
import group.teachingmanagerbk.mapper.MemberMapper;
import group.teachingmanagerbk.mapper.TermMapper;
import group.teachingmanagerbk.service.MemberService;
import group.teachingmanagerbk.service.TermService;
import group.teachingmanagerbk.utils.ReturnResult.ResultWithTotal;
import group.teachingmanagerbk.vo.course.Term;
import group.teachingmanagerbk.vo.member.Student;
import group.teachingmanagerbk.vo.member.Teacher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Slf4j
@Service
public class TermServiceImpl implements TermService {

    @Autowired
    TermMapper termMapper;


    @Override
    public ArrayList<Term> getTerms() {
        return termMapper.getTerms();
    }

    @Override
    public boolean insertTerm(Term json) {
        //需要先判断json中的数据是否为空
        if (checkTeacherBlank(json)) return false;
        termMapper.insertTerm(json);
        return true;
    }

    private  boolean checkTeacherBlank(Term term) {
        String year = term.getYear();
        String str1 = year.replaceAll(" ","");
        return "".equals(str1);
    }

    @Override
    public void updateCurrentTerm(String year) {
        termMapper.updateCurrentTerm(year);
    }

    @Override
    public String getCurrentTerm() {
        return termMapper.getCurrentTerm();
    }

}
