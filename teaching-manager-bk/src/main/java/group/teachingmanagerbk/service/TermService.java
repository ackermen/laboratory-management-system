package group.teachingmanagerbk.service;

import group.teachingmanagerbk.dto.member.QueryStudentParam;
import group.teachingmanagerbk.dto.member.QueryTeacherParam;
import group.teachingmanagerbk.utils.ReturnResult.ResultWithTotal;
import group.teachingmanagerbk.vo.course.Term;
import group.teachingmanagerbk.vo.member.Student;
import group.teachingmanagerbk.vo.member.Teacher;

import java.util.ArrayList;

/* 学期管理 */
public interface TermService {

    /**
     * 获取所有学期信息
     * @return 教师信息列表
     */
    ArrayList<Term> getTerms();

    /**
     * 插入一条学期信息
     * @param json  参数
     * @return true表示插入成功，否则不成功
     */
    boolean insertTerm(Term json);

    /**
     * 获取当前学期信息
     * @return 当前学期信息
     */
    String getCurrentTerm();

    /**
     * 获取当前学期信息
     */
    void updateCurrentTerm(String term);

}
