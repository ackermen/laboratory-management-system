package group.teachingmanagerbk.service;

import group.teachingmanagerbk.dto.application.ApplicationCourse;


public interface ApplicationCourseService {

    /**
     * 申请新的课程
     * @param course
     */
    void applyNewCourse(ApplicationCourse course);

    ApplicationCourse getCourseApplicationById(Long courseApplicationId);

    void updateApplication(ApplicationCourse applicationCourse);
}
