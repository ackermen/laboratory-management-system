package group.teachingmanagerbk.service;

import group.teachingmanagerbk.vo.repair.LabRepair;

import java.util.ArrayList;

public interface ApplicationRepairService {
    void applyNewRepair(LabRepair labRepair);

    ArrayList<LabRepair> getRepairApplicationByTeacherId(Integer teacherId);

    ArrayList<LabRepair> getApplicationByTeacherIdAndRepairStatusName(Integer teacherId, String repairStatusName);
}
