package group.teachingmanagerbk.vo.course;

import lombok.Data;

@Data
public class Term {
    private int termId;    //学期id
    private String year;   //学期
}