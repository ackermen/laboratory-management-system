package group.teachingmanagerbk.vo.member;

import lombok.Data;

@Data
public class Tester {
    private String testerId;            //实验员id
    private String testerNumber = "";   //实验员工号
    private String name = "";            //实验员姓名
    private String title = "";
}
