package group.teachingmanagerbk.vo.member;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 实验员对象导入VO
 *
 */

@Data
@NoArgsConstructor
// @Accessors(chain = true) // 导入不允许使用 会找不到set方法
public class TesterImportVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 实验员账号
     */
    @ExcelProperty(value = "实验员工号(账号)")
    private String testerNumber;
    @ExcelProperty(value = "实验员姓名")
    private String name;
    @ExcelProperty(value = "职称")
    private String title;

}
