package group.teachingmanagerbk.vo.member;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 教师对象导入VO
 *
 */

@Data
@NoArgsConstructor
// @Accessors(chain = true) // 导入不允许使用 会找不到set方法
public class TeacherImportVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 教师账号
     */
    @ExcelProperty(value = "教师工号(账号)")
    private String teacherNumber;
    @ExcelProperty(value = "教师姓名")
    private String name;
    @ExcelProperty(value = "职称")
    private String title;

}
