package group.teachingmanagerbk.vo.member;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor

public class Student implements Serializable {
    private String studentId;            //学生id
    private String studentNumber = "";   //学生学号
    private String name = "";            //学生姓名
    private String studentClass = "";    //班级
    private String profession = "";    //学生专业

}
