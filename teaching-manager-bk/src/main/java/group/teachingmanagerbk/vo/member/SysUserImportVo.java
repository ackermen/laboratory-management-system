package group.teachingmanagerbk.vo.member;

import com.alibaba.excel.annotation.ExcelProperty;
import group.teachingmanagerbk.utils.excel.ExcelDictFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;

/**
 * 学生对象导入VO
 *
 */

@Data
@NoArgsConstructor
// @Accessors(chain = true) // 导入不允许使用 会找不到set方法
public class SysUserImportVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 学生账号
     */
    @ExcelProperty(value = "学生学号(账号)")
    private String studentNumber;
    @ExcelProperty(value = "学生姓名")
    private String name;
    @ExcelProperty(value = "班级")
    private String studentClass;
    @ExcelProperty(value = "专业")
    private String profession;
}
