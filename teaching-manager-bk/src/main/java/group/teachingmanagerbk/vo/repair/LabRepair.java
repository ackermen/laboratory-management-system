package group.teachingmanagerbk.vo.repair;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LabRepair {
    int equipmentRepairId;
    int teacherId;
    String teacherName;
    String testerId;
    int labNum;
    String dateTime;
    String descriptionOfFault;
    String descriptionOfRepair;
    int repairStatusId;
    String year;
    String repairStatusName;
}
