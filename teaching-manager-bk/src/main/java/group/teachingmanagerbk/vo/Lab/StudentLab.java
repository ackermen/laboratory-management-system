package group.teachingmanagerbk.vo.Lab;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author scau-cwj
 * @date 2024/4/5
 * 实验类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentLab {
    int labBorrowingId;
    int studentId;
    String  studentName;
    int rootNum;
    String section;
    String startWeek;
    String endWeek;
    String reason;
    String status;
    String dateTime;
    String year;


}
