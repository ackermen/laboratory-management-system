package group.teachingmanagerbk.listener;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import group.teachingmanagerbk.service.MemberService;
import group.teachingmanagerbk.vo.member.Student;
import group.teachingmanagerbk.utils.excel.ExcelListener;
import group.teachingmanagerbk.utils.excel.ExcelResult;
import group.teachingmanagerbk.utils.file.exception.ServiceException;
import group.teachingmanagerbk.utils.excel.utils.SpringUtils;
import group.teachingmanagerbk.vo.member.SysUserImportVo;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * 系统用户自定义导入
 *
 * @author Lion Li
 */
@Slf4j
public class SysUserImportListener extends AnalysisEventListener<SysUserImportVo> implements ExcelListener<SysUserImportVo> {

    private final MemberService userService;

    private final Boolean isUpdateSupport;

    private int successNum = 0;
    private int failureNum = 0;
    private final StringBuilder successMsg = new StringBuilder();
    private final StringBuilder failureMsg = new StringBuilder();

    public SysUserImportListener(Boolean isUpdateSupport) {
        this.userService = SpringUtils.getBean(MemberService.class);
        this.isUpdateSupport = isUpdateSupport;
    }

    @Override
    public void invoke(SysUserImportVo userVo, AnalysisContext context) {
        Student user = this.userService.getStudentInfoByNumber(userVo.getStudentNumber());
        try {
            // 验证是否存在这个用户
            if (ObjectUtil.isNull(user)) {
                user = BeanUtil.toBean(userVo, Student.class);
//                ValidatorUtils.validate(user);
                userService.insertStudent(user);
                successNum++;
                successMsg.append("<br/>").append(successNum).append("、账号 ").append(user.getName()).append(" 导入成功");
            } else if (isUpdateSupport) {
                user = BeanUtil.toBean(userVo, Student.class);
//                ValidatorUtils.validate(user);
                userService.updateStudentInfoByNumber(user);
                successNum++;
                successMsg.append("<br/>").append(successNum).append("、账号 ").append(user.getName()).append(" 更新成功");
            } else {
//                failureNum++;
//                failureMsg.append("<br/>").append(failureNum).append("、账号 ").append(user.getName()).append(" 已存在");
                user = BeanUtil.toBean(userVo, Student.class);
//                ValidatorUtils.validate(user);
                userService.updateStudentInfoByNumber(user);
                successNum++;
                successMsg.append("<br/>").append(successNum).append("、账号 ").append(user.getName()).append(" 更新成功");
            }
        } catch (Exception e) {
            failureNum++;
            String msg = "<br/>" + failureNum + "、账号 " + user.getName() + " 导入失败：";
            failureMsg.append(msg).append(e.getMessage());
            log.error(msg, e);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }

    @Override
    public ExcelResult<SysUserImportVo> getExcelResult() {
        return new ExcelResult<SysUserImportVo>() {

            @Override
            public String getAnalysis() {
                if (failureNum > 0) {
                    failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
                    throw new ServiceException(failureMsg.toString());
                } else {
                    successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
                }
                return successMsg.toString();
            }

            @Override
            public List<SysUserImportVo> getList() {
                return null;
            }

            @Override
            public List<String> getErrorList() {
                return null;
            }
        };
    }
}
