package group.teachingmanagerbk.dto.application;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationCourse {
    private Long courseApplicationId;
    private Long teacherId;
    private String teacherName;
    private String courseName;
    private String time;        //节次
    private String studentClass;    //学生班级
    private Integer startWeek;
    private Integer endWeek;
    private Long studentNum;
    private String labType;
    private String year;
    private String weekday;
    private Integer labNum;
    private Long courseExaminationId;     //审批状态id
    private String courseExaminationName;   //审批状态名称
    private Long operationId;             //操作id
    private String operationName;           //操作名称

}
