package group.teachingmanagerbk.dto.lab;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Laboratory {
    private Long laboratoryId;
    private String labName;
    private String labType;
    private Integer labNum;
    private Integer number;
    private Long testId;
    private String year;
}
