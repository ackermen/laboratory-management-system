package group.teachingmanagerbk.dto.lab;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LabSchedules {
    private Long labSchedulesId;
    private String weekday;
    private String labName;
    private Integer labNum;
    private String time;
    private String courseName;
    private String teacherName;
    private String studentClass;
    private String weeks;
    private String year;
    private Integer startWeek;
    private Integer endWeek;
}
