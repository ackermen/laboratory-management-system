package group.teachingmanagerbk.dto.member;

import group.teachingmanagerbk.vo.member.Tester;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 查询实验员信息的类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class QueryTesterParam {
    private Integer pageSize;
    private Integer currentPage;
    private Tester param;
}
