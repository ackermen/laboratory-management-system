package group.teachingmanagerbk.mapper;

import group.teachingmanagerbk.vo.Lab.StudentLab;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

import group.teachingmanagerbk.dto.lab.Laboratory;

import java.util.ArrayList;

/**
 * @author scau-cwj
 * @date 2024/4/5
 */
@Mapper
public interface LabMapper {
    /**
     * @author: SCAU_CWJ
     * @date: 2024/4/5
     * @description: 根据学生id获取申请实验室信息
     */
        @Select("select * from lab_borrowing where student_id = #{id}")
        List<StudentLab> getStudentInfo(int id);
    boolean update(StudentLab lab);
    boolean add(StudentLab lab);


    @Select("select * from laboratory")
    ArrayList<Laboratory> getLab();

    @Select("select * from laboratory where lab_type = #{labType}")
    ArrayList<Laboratory> getLabByLabType(String labType);
    boolean accpet(int labBorrowingId);


    @Select("select tester_id from laboratory where lab_num = #{labNum}")
    String getTesterIdByLabNum(int labNum);

    @Select("select repair_status_id from repair_status where repair_status_name = #{repairStatusName}")
    Integer getRepairStatusIdByStatusName(String repairStatusName);
}
