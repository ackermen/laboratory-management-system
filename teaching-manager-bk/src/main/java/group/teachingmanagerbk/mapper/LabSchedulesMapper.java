package group.teachingmanagerbk.mapper;

import group.teachingmanagerbk.dto.application.ApplicationCourse;
import group.teachingmanagerbk.dto.lab.LabSchedules;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.ArrayList;

@Mapper
public interface LabSchedulesMapper {
    @Insert("insert into lab_schedules (weekday, lab_name, lab_num, time, course_name, teacher_name, student_class, weeks, year,start_week,end_week) VALUES " +
            "(#{weekday},#{labName},#{labNum},#{time},#{courseName},#{teacherName},#{studentClass},#{weeks},#{year},#{startWeek},#{endWeek})")
    void insertNewSchedule(LabSchedules l);

    @Select("select * from lab_schedules where weekday = #{weekday} and time = #{time} and year = #{year} and lab_num = #{labNum} and " +
            "( (start_week <= #{endWeek} and end_week >= #{endWeek}) or (start_week <= #{startWeek} and end_week >= #{startWeek}) )")
    ArrayList<LabSchedules> judge(ApplicationCourse course);

    @Select("select * from lab_schedules where year = #{year}")
    ArrayList<LabSchedules> getLabSchedulesByYear(String year);
}
