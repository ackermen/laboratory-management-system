package group.teachingmanagerbk.mapper;

import group.teachingmanagerbk.vo.repair.LabRepair;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.ArrayList;

@Mapper
public interface ApplicationRepairMapper {

    @Insert("insert into equipment_repair(teacher_id, teacher_name, lab_num,tester_id , date_time, description_of_fault, repair_status_id, year) " +
            "values (#{teacherId},#{teacherName},#{labNum},#{testerId},#{dateTime},#{descriptionOfFault},#{repairStatusId},#{year})")
     void addNewRepair(LabRepair labRepair);

    @Select("select e.*,r.repair_status_name from equipment_repair as e left join repair_status as r on e.repair_status_id = r.repair_status_id where e.teacher_id = #{teacherId} and e.year = #{year}")
    ArrayList<LabRepair> getRepairApplicationByTeacherId(Integer teacherId, String year);

    @Select("select e.*,r.repair_status_name from equipment_repair as e left join repair_status as r on e.repair_status_id = r.repair_status_id" +
            " where e.teacher_id = #{teacherId} and e.year = #{year} and  e.repair_status_id = #{repairStatusId}")
    ArrayList<LabRepair> getApplicationByTeacherIdAndRepairStatusName(Integer teacherId, Integer repairStatusId, String year);
}
