package group.teachingmanagerbk.mapper;

import group.teachingmanagerbk.vo.Lab.StudentLab;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.ArrayList;

@Mapper
public interface BorrowExaminationMapper {

    /* 查询所有审批的记录 */
    @Select("select * from lab_borrowing where year = #{year}")
    ArrayList<StudentLab> getBorrowExamination(String year);

    /* 更新申请的审批状态信息 */
    @Update("update lab_borrowing set status = #{status} " +
            "where lab_borrowing_id = #{labBorrowingId}")
    void rejectBorrowExamination(String status,int labBorrowingId);

    /* 更新申请的课程id和审批状态信息 */
    @Update("update lab_borrowing set status = #{status} " +
            "where lab_borrowing_id = #{labBorrowingId}")
    void agreeBorrowExamination(String status,int labBorrowingId);

}
