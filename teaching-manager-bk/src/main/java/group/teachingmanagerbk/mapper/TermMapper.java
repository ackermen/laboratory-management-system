package group.teachingmanagerbk.mapper;

import group.teachingmanagerbk.vo.course.Term;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;


import java.util.ArrayList;

@Mapper
public interface TermMapper {

    /* 更新所有学期信息 */
    @Select("select * from term "
            )
    ArrayList<Term> getTerms();

    /* 插入一条教师信息 */
    @Insert("insert into term (year) values (#{year})")
    void insertTerm(Term term);

    /* 查询当前学期信息 */
    @Select("select year from administrator where administrator_id = 1"
    )
    String getCurrentTerm();

    /* 更新当前学期信息 */
    @Update("update administrator set year = #{term} where administrator_id = 1")
    void updateCurrentTerm(String term);
}
