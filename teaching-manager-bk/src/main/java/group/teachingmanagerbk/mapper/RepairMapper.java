package group.teachingmanagerbk.mapper;

import group.teachingmanagerbk.vo.repair.LabRepair;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.ArrayList;

@Mapper
public interface RepairMapper {

    // 查询所有维修记录
    @Select("select * from equipment_repair " +
            "where year = #{currentTerm} and tester_id = #{curId}")
    ArrayList<LabRepair> getLabRepair(String currentTerm, String curId);

    // 更新进行中的维修记录
    @Update("update equipment_repair set repair_status_id = #{repairStatusId} " +
            "where equipment_repair_id = #{equipmentRepairId}")
    void startRepair(int repairStatusId, int equipmentRepairId);

    // 更新完成的维修记录
    @Update("update equipment_repair " +
            "set repair_status_id = #{repairStatusId} , description_of_repair = #{descriptionOfRepair} " +
            "where equipment_repair_id = #{equipmentRepairId}")
    void finishRepair(String descriptionOfRepair, int repairStatusId, int equipmentRepairId);

    @Select("select e.*,r.repair_status_name from equipment_repair as e left join repair_status as r on e.repair_status_id = r.repair_status_id where equipment_repair_id = #{repairId}")
    LabRepair getRepairById(Integer repairId);
}
