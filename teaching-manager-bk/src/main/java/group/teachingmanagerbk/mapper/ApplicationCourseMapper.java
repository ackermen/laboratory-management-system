package group.teachingmanagerbk.mapper;

import group.teachingmanagerbk.dto.application.ApplicationCourse;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface ApplicationCourseMapper {

    @Insert("insert into course_application(teacher_id, teacher_name, course_name, time, student_class, start_week, end_week, lab_type, course_examination_id, year, operation_id, weekday,student_num) " +
            "values (#{teacherId},#{teacherName},#{courseName},#{time},#{studentClass},#{startWeek},#{endWeek},#{labType},#{courseExaminationId},#{year},#{operationId},#{weekday},#{studentNum})")
    void insertNewCourse(ApplicationCourse course);

    @Select("select a.*, ce.name as courseExaminationName, o.name as operationName from course_application a " +
            "left join course_examination ce on a.course_examination_id = ce.course_examination_id " +
            "left join operation o on o.operation_id = a.operation_id " +
            "where course_application_id = #{courseApplicationId} and a.year = #{year}")
    ApplicationCourse getCourseApplicationById(Long courseApplicationId, String year);

    @Update("update course_application set lab_num = #{labNum} where course_application_id = #{courseApplicationId}")
    void updateLabNum(Long courseApplicationId, Integer labNum);

    @Update("update course_application set course_name = #{courseName},time = #{time},student_class=#{studentClass},start_week = #{startWeek}," +
            "end_week = #{endWeek},lab_type = #{labType},student_num=#{studentNum},weekday=#{weekday} where course_application_id = #{courseApplicationId}")
    void updateApplication(ApplicationCourse applicationCourse);
}
