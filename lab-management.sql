/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : localhost:3306
 Source Schema         : lab-management

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : 65001

 Date: 27/04/2024 01:15:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for administrator
-- ----------------------------
DROP TABLE IF EXISTS `administrator`;
CREATE TABLE `administrator`  (
  `administrator_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `account` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `password` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '123456',
  `year` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`administrator_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of administrator
-- ----------------------------
INSERT INTO `administrator` VALUES (1, 'admin', '管理员', '123456', '2024-2025-1');

-- ----------------------------
-- Table structure for course_application
-- ----------------------------
DROP TABLE IF EXISTS `course_application`;
CREATE TABLE `course_application`  (
  `course_application_id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) NOT NULL COMMENT '教师id',
  `teacher_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '教师名',
  `course_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '课程名',
  `time` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '节次',
  `student_class` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '班级',
  `start_week` int(4) NULL DEFAULT NULL COMMENT '起始周',
  `end_week` int(4) NULL DEFAULT NULL COMMENT '结束周',
  `lab_type` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '实验室类型',
  `course_examination_id` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '申请状态',
  `year` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '年度',
  `operation_id` int(11) NULL DEFAULT NULL,
  `student_num` int(8) NULL DEFAULT NULL COMMENT '学生人数',
  `weekday` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '上课星期',
  `lab_num` int(5) NULL DEFAULT NULL COMMENT '实验室编号',
  PRIMARY KEY (`course_application_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of course_application
-- ----------------------------
INSERT INTO `course_application` VALUES (26, 1, 'lucy', '汇编语言', '3-5', '软工1班', 2, 6, '软件', '2', '2024-2025-1', 1, 40, '星期二', 732);
INSERT INTO `course_application` VALUES (27, 1, 'lucy', '计算机科学', '1-2', '软工2班', 2, 6, '软件', '2', '2024-2025-1', 1, 50, '星期四', 732);
INSERT INTO `course_application` VALUES (28, 2, 'jack', 'c语言程序设计', '13-15', '软工1班', 2, 10, '软件', '2', '2024-2025-1', 1, 50, '星期二', 732);
INSERT INTO `course_application` VALUES (29, 1, 'lucy', '现代软件开发技术', '10-12', '软工7班', 1, 11, '软件', '1', '2024-2025-1', 1, 50, '星期二', NULL);

-- ----------------------------
-- Table structure for course_examination
-- ----------------------------
DROP TABLE IF EXISTS `course_examination`;
CREATE TABLE `course_examination`  (
  `course_examination_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`course_examination_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of course_examination
-- ----------------------------
INSERT INTO `course_examination` VALUES (1, '待审批');
INSERT INTO `course_examination` VALUES (2, '已通过');
INSERT INTO `course_examination` VALUES (3, '未通过');

-- ----------------------------
-- Table structure for equipment_repair
-- ----------------------------
DROP TABLE IF EXISTS `equipment_repair`;
CREATE TABLE `equipment_repair`  (
  `equipment_repair_id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) NULL DEFAULT NULL,
  `teacher_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '教师名',
  `tester_id` int(11) NULL DEFAULT NULL COMMENT '实验员id',
  `lab_num` int(11) NULL DEFAULT NULL COMMENT '编号',
  `date_time` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '报修日期',
  `description_of_fault` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '故障描述',
  `description_of_repair` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '维修说明',
  `repair_status_id` int(20) NULL DEFAULT NULL COMMENT '维修状态',
  `year` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '年度',
  PRIMARY KEY (`equipment_repair_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of equipment_repair
-- ----------------------------
INSERT INTO `equipment_repair` VALUES (18, 2, 'jack', 1, 734, '2024-4-25', '电脑无法开机', '电脑主板坏掉', 4, '2024-2025-1');
INSERT INTO `equipment_repair` VALUES (19, 2, 'jack', 3, 806, '2024-4-25', '电脑无法连接网络', '网线松掉', 3, '2024-2025-1');
INSERT INTO `equipment_repair` VALUES (20, 1, 'lucy', 2, 708, '2024-4-26', '机器故障', '零件损坏，以更换', 3, '2024-2025-1');

-- ----------------------------
-- Table structure for lab_borrowing
-- ----------------------------
DROP TABLE IF EXISTS `lab_borrowing`;
CREATE TABLE `lab_borrowing`  (
  `lab_borrowing_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NULL DEFAULT NULL,
  `student_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `root_num` int(5) NULL DEFAULT NULL,
  `weeks` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '申请原因',
  `status` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '申请状态',
  `date_time` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '填报日期',
  `year` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `section` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `start_week` int(3) NULL DEFAULT NULL,
  `end_week` int(3) NULL DEFAULT NULL,
  PRIMARY KEY (`lab_borrowing_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lab_borrowing
-- ----------------------------
INSERT INTO `lab_borrowing` VALUES (27, 1, '张三', 708, NULL, '做机器学习', '不通过', '2024-4-25 21:47:2', '2024-2025-1', '1-2', 2, 4);
INSERT INTO `lab_borrowing` VALUES (28, 1, '张三', 704, NULL, '进行比赛', '通过', '2024-4-25 21:49:37', '2024-2025-1', '3-5', 1, 2);
INSERT INTO `lab_borrowing` VALUES (29, 1, '张三', 732, NULL, '进行测试', '通过', '2024-4-25 22:6:54', '2024-2025-1', '6-7', 2, 3);
INSERT INTO `lab_borrowing` VALUES (30, 1, '张三', 806, NULL, '借用考试', '审核中', '2024-4-26 11:6:21', '2024-2025-1', '6-7', 1, 2);

-- ----------------------------
-- Table structure for lab_schedules
-- ----------------------------
DROP TABLE IF EXISTS `lab_schedules`;
CREATE TABLE `lab_schedules`  (
  `lab_schedules_id` int(11) NOT NULL AUTO_INCREMENT,
  `weekday` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '排课星期',
  `lab_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '实验室名称',
  `lab_num` int(5) NULL DEFAULT NULL COMMENT '编号',
  `time` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '节次',
  `course_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '课程名',
  `teacher_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '教师名',
  `student_class` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '班级',
  `weeks` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '周次',
  `year` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '学年',
  `start_week` int(4) NULL DEFAULT NULL COMMENT '起始周',
  `end_week` int(4) NULL DEFAULT NULL COMMENT '结束周',
  PRIMARY KEY (`lab_schedules_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lab_schedules
-- ----------------------------
INSERT INTO `lab_schedules` VALUES (9, '星期二', '软件实验室', 732, '3-5', '汇编语言', 'lucy', '软工1班', '2-6', '2024-2025-1', 2, 6);
INSERT INTO `lab_schedules` VALUES (10, '星期四', '软件实验室', 732, '1-2', '计算机科学', 'lucy', '软工2班', '2-6', '2024-2025-1', 2, 6);
INSERT INTO `lab_schedules` VALUES (11, '星期二', '软件实验室', 732, '13-15', 'c语言程序设计', 'jack', '软工1班', '2-10', '2024-2025-1', 2, 10);

-- ----------------------------
-- Table structure for laboratory
-- ----------------------------
DROP TABLE IF EXISTS `laboratory`;
CREATE TABLE `laboratory`  (
  `laboratory_id` int(11) NOT NULL AUTO_INCREMENT,
  `lab_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `lab_type` varchar(11) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '实验室类型',
  `lab_num` int(5) NULL DEFAULT NULL COMMENT '实验室编号',
  `number` int(11) NULL DEFAULT NULL,
  `tester_id` int(11) NULL DEFAULT NULL COMMENT '实验员',
  PRIMARY KEY (`laboratory_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of laboratory
-- ----------------------------
INSERT INTO `laboratory` VALUES (1, '软件实验室', '软件', 732, 90, 1);
INSERT INTO `laboratory` VALUES (2, '软件实验室', '软件', 734, 80, 1);
INSERT INTO `laboratory` VALUES (3, '计算机系统实验室', '硬件', 704, 80, 2);
INSERT INTO `laboratory` VALUES (4, '计算机系统实验室', '硬件', 708, 80, 2);
INSERT INTO `laboratory` VALUES (5, '物联网实验室', '网络', 806, 70, 3);
INSERT INTO `laboratory` VALUES (6, '物联网实验室', '网络', 808, 80, 3);

-- ----------------------------
-- Table structure for operation
-- ----------------------------
DROP TABLE IF EXISTS `operation`;
CREATE TABLE `operation`  (
  `operation_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`operation_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of operation
-- ----------------------------
INSERT INTO `operation` VALUES (1, '新增');
INSERT INTO `operation` VALUES (2, '修改');
INSERT INTO `operation` VALUES (3, '删除');

-- ----------------------------
-- Table structure for repair_status
-- ----------------------------
DROP TABLE IF EXISTS `repair_status`;
CREATE TABLE `repair_status`  (
  `repair_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `repair_status_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`repair_status_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of repair_status
-- ----------------------------
INSERT INTO `repair_status` VALUES (1, '未维修');
INSERT INTO `repair_status` VALUES (2, '维修中');
INSERT INTO `repair_status` VALUES (3, '已维修');
INSERT INTO `repair_status` VALUES (4, '无法维修');

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `student_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '账号(学号)',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `student_class` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `password` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '123456',
  `profession` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '专业',
  PRIMARY KEY (`student_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES (1, '101', '张三', '软工1班', '123456', '软件工程');
INSERT INTO `student` VALUES (2, '102', '李四', '软工2班', '123456', '软件工程');
INSERT INTO `student` VALUES (3, '103', '王五', '计机1班', '123456', '计算机科学与技术');
INSERT INTO `student` VALUES (4, '104', '赵六', '计机2班', '123456', '计算机科学与技术');

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher`  (
  `teacher_id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '工号',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `title` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `password` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '123456',
  PRIMARY KEY (`teacher_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO `teacher` VALUES (1, '1001', 'lucy', '教授', '123456');
INSERT INTO `teacher` VALUES (2, '1002', 'jack', '副教授', '123456');
INSERT INTO `teacher` VALUES (3, '1003', 'mary', '副教授', '123456');

-- ----------------------------
-- Table structure for term
-- ----------------------------
DROP TABLE IF EXISTS `term`;
CREATE TABLE `term`  (
  `term_id` int(11) NOT NULL AUTO_INCREMENT,
  `year` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`term_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of term
-- ----------------------------
INSERT INTO `term` VALUES (1, '2023-2024-1');
INSERT INTO `term` VALUES (8, '2024-2025-1');
INSERT INTO `term` VALUES (9, '2025-2026-1');

-- ----------------------------
-- Table structure for tester
-- ----------------------------
DROP TABLE IF EXISTS `tester`;
CREATE TABLE `tester`  (
  `tester_id` int(11) NOT NULL AUTO_INCREMENT,
  `tester_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '实验员工号',
  `password` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '123456',
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '职称',
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`tester_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tester
-- ----------------------------
INSERT INTO `tester` VALUES (1, '10001', '123456', '高级技工', '小明');
INSERT INTO `tester` VALUES (2, '10002', '123456', '普通技工', '小王');
INSERT INTO `tester` VALUES (3, '10003', '123456', '普通技工', '小强');

SET FOREIGN_KEY_CHECKS = 1;
