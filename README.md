# 实验室管理系统 & 课程课室管理系统

#### 介绍
现代软件开发技术—实验室管理系统（master 分支）

数据库课程设计—课程课室管理系统（db_version 分支）

#### 软件架构
系统所用技术栈：
前端：vue2，axios, element
后端：spring，maven，mybatis

数据库操作
在正式启动项目之前，需要先在自己的MySQL中创建一个名叫lab-management数据库，
运行该目录下的那个sql脚本，创建基本的表结构

前后端常用文件所在路径
后端： .\teaching-manager-bk\src\main\resources\application.properties 	（注：用来设置数据库连接、日志输出等配置信息）
前端：.\teaching-manager-ui\src\utils\request.js	  (注：用来设置对后端资源的基本请求路径，请求头等信息）
在正式运行项目时，需要将上面两个文件中的一些信息进行修改。
后端的那个文件需要修改数据库的连接信息，前端那个文件需要修改文件中的baseURL（对后端资源的基本请求路径）

#### 安装教程

1.  jdk17
2.  npm 10.2.3 

#### 使用说明
0.  配置数据库源，运行sql文件导入数据到数据库中
1.  cd 进入ui目录 npm install, npm run serve启动前端
2.  cd 进入back目录 maven安装依赖，启动springboot后端

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
